import React from 'react';
import avatarImage from './assets/me.png';
import {scrollTo} from './utils.js';
import ReactDOM from 'react-dom';
import './About.css';

class About extends React.Component{
    constructor(props){
        super(props);
        this.state={
            onNextClick : props.onNextClick
        };
    }

    onNextClick(to){
        if(to === 'story1'){
            var story1 = this.refs.story1;
            var story1DOM = ReactDOM.findDOMNode(story1);
            var pos = story1DOM.offsetTop + story1DOM.offsetHeight;
            scrollTo(document.body, pos, 1000);
        }else if(to === 'story2'){
            var story2 = this.refs.story2;
            var story2DOM = ReactDOM.findDOMNode(story2);
            var pos = story2DOM.offsetTop + story2DOM.offsetHeight;
            scrollTo(document.body, pos, 1000);
        } else if(to === 'projects'){
            this.state.onNextClick();            
        }
    }

    renderPhoneLayout(){
        return (
            <div className="about-page-wrapper">
                <div ref="aboutPage" className="about-page">
                    <div ref="aboutCard" className="card">
                        <section className="sidebar">
                            <img src={avatarImage} alt="avatar" className="avatar"/>
                            <div>
                                <h3>Software Developer</h3>
                                <h3>Web Padawan</h3>
                                <div className="sidebar-text-wrapper">
                                    <h4>Born in Brazil. Living in Australia. (23)</h4>
                                    <h4>I studied Bachelor of Computer Science, Information Systems and Digital Design. Tired of studying. It’s time to get my hands dirty.</h4>
                                    <h4>Technology enthusiast. I love what I do.</h4>
                                    <h4>Powers of the force make me believe that anything is possible. There is no such problem that doesn’t have a solution.</h4>
                                    <h4>Power to the crowd!</h4>
                                </div>
                            </div>
                            <div className="next-logo-container">
                                <div className="next-logo-wrapper" ref="nextButton" 
                                    onClick={() => this.onNextClick('story1')}>
                                    <svg className='next-logo' xmlns="http://www.w3.org/2000/svg" 
                                            width="50" height="50" viewBox="0 0 94 94">
                                        <circle id='circle' className="next-logo-circle" 
                                            stroke="#212121" cx="47" cy="47" r="45"/>
                                        <path id='chevron' className="next-logo-chevron" 
                                            fill="#212121" d="M69.5,38.39l-5.29-5.29L47,50.28,29.79,33.11,24.5,38.39,47,60.89Z"/>
                                    </svg>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <div ref="story1" className="about-page">
                    <div ref="storyCard1" className="card">
                        <section className="content">
                                <div className="about-content-header">
                                    <div className="line"/>
                                    <h3>What made me a Padawan</h3>
                                </div>
                                <div className="about-content-content">
                                    <h4>800 years ago, a force has been awake inside of me. Since there, I’ve started to code and see things differently. Trained by masters, I’ve been taught that there are no limits for the human brain. The force is inside of us.</h4>
                                    <h4>This force was discovered when I had to travel to dark places, visiting caves and places where the sun had never set. At that moment, Pascal and C++ were the only allies I had. And for a long time, towards many galaxies I had to keep travelling, making new allies and learning more about how to control the force.</h4>
                                </div>
                                <div className="next-logo-container">
                                    <div className="next-logo-wrapper" ref="nextButton" 
                                        onClick={() => this.onNextClick('story2')}>
                                        <svg className='next-logo' xmlns="http://www.w3.org/2000/svg" 
                                                width="50" height="50" viewBox="0 0 94 94">
                                            <circle id='circle' className="next-logo-circle" 
                                                stroke="#212121" cx="47" cy="47" r="45"/>
                                            <path id='chevron' className="next-logo-chevron" 
                                                fill="#212121" d="M69.5,38.39l-5.29-5.29L47,50.28,29.79,33.11,24.5,38.39,47,60.89Z"/>
                                        </svg>
                                    </div>
                                </div>
                        </section>
                    </div>
                </div>
                <div ref="story2" className="about-page">
                    <div ref="storyCard2" className="card">
                        <section className="content">
                            <div className="about-content-header">
                                <div className="line"/>
                                <h3>...</h3>
                            </div>
                            <div className="about-content-content">
                               <h4>Many years later, I personally had to deal and fought in different battles of the supreme war. In fact, I had two ways to go; Follow the force and stay stronger than never or abandon all the teaching that I had in order to quit the war and live a peaceful life. Then, I chose to become stronger.</h4>
                                <h4>After my decision, The Legendary Functional Master became my new lord. He made me way tougher and capable of controlling the force with anything I had. I didn’t need lightsabers or anything to be strong. I could feel strong only with my own hands.</h4>
                                <h4>And a few years ago, I went on a spiritual journey where I hope to find all the answers I need to become a Jedi.</h4>
                            </div>
                            <div className="next-logo-container">
                                <div className="next-logo-wrapper" ref="nextButton" 
                                    onClick={() => this.onNextClick('projects')}>
                                    <svg className='next-logo' xmlns="http://www.w3.org/2000/svg" 
                                        width="50" height="50" viewBox="0 0 94 94">
                                        <circle id='circle' className="next-logo-circle" 
                                            stroke="#212121" cx="47" cy="47" r="45"/>
                                        <path id='chevron' className="next-logo-chevron" 
                                           fill="#212121" d="M69.5,38.39l-5.29-5.29L47,50.28,29.79,33.11,24.5,38.39,47,60.89Z"/>
                                    </svg>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                
            </div>
        );
    }

    renderDesktopLayout(){
        return (
            <div className="about-page-wrapper">
                <div className="about-page">
                    <div className="card">
                        <section className="sidebar">
                            <img src={avatarImage} alt="avatar" className="avatar"/>
                            <div>
                                <h3>Software Developer</h3>
                                <h3>Web Padawan</h3>
                                <div className="sidebar-text-wrapper">
                                    <h4>Born in Brazil. Living in Australia. (23)</h4>
                                    <h4>I studied Bachelor of Computer Science, Information Systems and Digital Design. Tired of studying. It’s time to get my hands dirty.</h4>
                                    <h4>Technology enthusiast. I love what I do.</h4>
                                    <h4>Powers of the force make me believe that anything is possible. There is no such problem that doesn’t have a solution.</h4>
                                    <h4>Power to the crowd!</h4>
                                </div>
                            </div>
                        </section>
                        <section className="content">
                            <div className="about-content-header">
                                <div className="line"/>
                                <h3>What made me a Padawan</h3>
                            </div>
                            <div className="about-content-content">
                                <h4>800 years ago, a force has been awake inside of me. Since there, I’ve started to code and see things differently. Trained by masters, I’ve been taught that there are no limits for the human brain. The force is inside of us.</h4>
                                <h4>This force was discovered when I had to travel to dark places, visiting caves and places where the sun had never set. At that moment, Pascal and C++ were the only allies I had. And for a long time, towards many galaxies I had to keep travelling, making new allies and learning more about how to control the force.</h4>
                                <h4>Many years later, I personally had to deal and fought in different battles of the supreme war. In fact, I had two ways to go; Follow the force and stay stronger than never or abandon all the teaching that I had in order to quit the war and live a peaceful life. Then, I chose to become stronger.</h4>
                                <h4>After my decision, The Legendary Functional Master became my new lord. He made me way tougher and capable of controlling the force with anything I had. I didn’t need lightsabers or anything to be strong. I could feel strong only with my own hands.</h4>
                                <h4>And a few years ago, I went on a spiritual journey where I hope to find all the answers I need to become a Jedi.</h4>
                            </div>
                        </section>
                    </div>
                </div>
                <div className="next-logo-wrapper" ref="nextButton" 
                        onClick={() => this.onNextClick()}>
                        <svg className='next-logo' xmlns="http://www.w3.org/2000/svg" 
                                width="50" height="50" viewBox="0 0 94 94">
                            <circle id='circle' className="next-logo-circle" 
                                stroke="#212121" cx="47" cy="47" r="45"/>
                            <path id='chevron' className="next-logo-chevron" 
                                fill="#212121" d="M69.5,38.39l-5.29-5.29L47,50.28,29.79,33.11,24.5,38.39,47,60.89Z"/>
                    </svg>
                </div>
            </div>
        );
    }

    render(){
        if(document.body.clientWidth <= 360){
            return this.renderPhoneLayout();
        }
        else {
            return this.renderDesktopLayout();
        }
    }
}

export default About;