import React from 'react';
import firebase from 'firebase';
import {convertJSONObjectToJSONArray} from './utils.js';

class Projects extends React.Component{
    constructor(props){
        super(props);
        this.state={
            projectTypeSelected : 'web',
            projects: []
        }
    }

    renderProjectItem(item){
        return <div className="project">
                    {item.title}
                    {console.log(item.title)}
                    <div className="bar"></div>
                </div>
    }

    renderProjects(){
        return this.state.projects.map(
                projectsTypeItem => {
                    if(projectsTypeItem.key == this.state.projectTypeSelected) {
                        var array = convertJSONObjectToJSONArray(projectsTypeItem);
                        console.log(array);
                        return array.map(
                            projectItemCandidate => {
                                if(typeof(projectItemCandidate) == 'object'){
                                    return this.renderProjectItem(projectItemCandidate);
                                }
                            }, this);
                        }   
                    }, this);
    }

    renderProjectsContent(){
        return <div>
                    <h2>{this.state.projectTypeSelected}</h2>
                    <div className="projects-wrapper">
                        {this.renderProjects()}
                    </div>
            </div>;
    }

    onProjectsLinkClick(itemKey){
        this.setState({projectTypeSelected: itemKey});
    }

    componentDidMount(){
        var dbProjectsRef = firebase.database().ref().child('projects');
        dbProjectsRef.on('value', snap => {
            const val = snap.val();
            var array = convertJSONObjectToJSONArray(val);
            this.setState({projects : array});
        });
    }

    renderSidebarMenuItem(item){
        return <li key={item.key} onClick={()=>this.onProjectsLinkClick(item.key)}>
                    <h3>{item.key}</h3>
                </li>;
    }

    renderSidebarMenu(){
        return <ul>
                  {this.state.projects.map(item => {
                    return this.renderSidebarMenuItem(item);
                  })}
               </ul>;
    }

    render(){
        return (
            <div className="projects-page">
                <div className="card">
                    <section className="content">
                        {this.renderProjectsContent()}
                    </section>
                    <section className="sidebar">
                        <h2>Projects &</h2>
                        <h2>Experiences</h2>
                        {this.renderSidebarMenu()}
                    </section>
                </div>
            </div>
        );
    }
}

export default Projects;