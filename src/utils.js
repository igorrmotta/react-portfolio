var raf = require('raf');

var easeInOutQuad = function (t, b, c, d) {
    t /= d / 2;
    if (t < 1) {
        return c / 2 * t * t + b;
    }
    t--;
    return -c / 2 * (t * (t - 2) - 1) + b;
};

/**
* Scroll to a DOM element
* @param {Element} element  - The element to scroll to.
* @param {number}  to       - The position to scroll to, relative to the top
*                            of the element.
* @param {number}  duration - How long the scrolling should take.
* @param {boolean} horizontal - The direction of the scroll (default false)
*/
function scrollTo(element, to, duration, horizontal = false) {
    var start = horizontal ? element.scrollLeft : element.scrollTop,
        change = to - start,
        currentTime = 0,
        increment = 10;

    var animateScroll = function(){
        currentTime += increment;
        var val = easeInOutQuad(currentTime, start, change, duration);
        if(horizontal){
            element.scrollLeft = val;
        } else{
            element.scrollTop = val;            
        }
        if(currentTime < duration) {
            raf(animateScroll);
        }
    };
    animateScroll();
};

function convertJSONObjectToJSONArray(jsonObject){
     var array = Object.keys(jsonObject).map(
                function (key) {
                    var obj = jsonObject[key];
                    if(typeof(obj) == 'object')
                        obj.key = key;
                    return obj;
                }
    );

    return array;
}

export {scrollTo};
export {convertJSONObjectToJSONArray};