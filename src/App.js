import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import Cover from './Cover.js';
import About from './About.js';
import Projects from './Projects.js';
import {scrollTo} from './utils.js';

class App extends Component {

  onCoverNextClick(){
    var aboutPage = this.refs.aboutPage;
    var aboutDOMNode = ReactDOM.findDOMNode(aboutPage);
    var topPos = aboutDOMNode.offsetTop;
    scrollTo(document.body, topPos, 1000);
  }

  onAboutNextClick(){
    var projectsPage = this.refs.projectsPage;
    var projectsDOMNode = ReactDOM.findDOMNode(projectsPage);
    var topPos = projectsDOMNode.offsetTop;
    scrollTo(document.body, topPos, 1000);
  }

  render() {
    return (
      <div>
        <Cover ref="coverPage" 
              onNextClick={this.onCoverNextClick.bind(this)}></Cover>
        <About ref="aboutPage"
              onNextClick={this.onAboutNextClick.bind(this)}></About>
        <Projects ref="projectsPage"></Projects>
      </div>
    );
  }
}

export default App;
