import React from 'react';
import igLogo from './assets/ig.svg';
import orrmottaLogo from './assets/orrmotta.svg';
import './Cover.css';

class Cover extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            onNextClick: props.onNextClick,
            underlineTimeInterval: null,
            bExpanded: false            
        };
    }

    onClick() {
        this.insertSecondElement();
    }

    onNextClick(){
        this.state.onNextClick();
    }
        
    onFirstElementClick(){
        if(document.body.clientWidth <= 360){
            this.refs.firstElement.style.height = '50px';
            this.refs.firstElementLogo.style.height = '50px';
        }else if(document.body.clientWidth <= 360){
            this.refs.firstElement.style.height = '65px';
            this.refs.firstElementLogo.style.height = '65px';
        } else if(document.body.clientWidth <= 480){
            this.refs.firstElement.style.height = '75px';
            this.refs.firstElementLogo.style.height = '75px';
        }

        this.refs.firstElement.style.marginLeft = '0';


        this.refs.secondElement.style.visibility = 'visible';
        this.refs.secondElement.style.opacity = '1';

        this.refs.logoSubtitle.style.visibility = 'visible';
        this.refs.logoSubtitle.style.opacity = '1';

        this.refs.nextButton.style.visibility = 'visible';
        this.refs.nextButton.style.opacity = '1';

        var svgObj = document.getElementById('svgObject');
        var svgDoc = svgObj.contentDocument;
        var svgItem = svgDoc.getElementById('underline');


        if (this.state.underlineTimeInterval)
            clearInterval(this.state.underlineTimeInterval);

            this.setState({
                underlineTimeInterval : setInterval(function() {
                    if (svgItem.style.opacity === '0')
                        svgItem.style.opacity = 1
                    else
                        svgItem.style.opacity = 0;
                }, 1000)
        });
    }

    renderFirstElement(){
        return <div ref="firstElement" className="shadow">
                    <img ref="firstElementLogo"
                        className="first-element"
                        src={igLogo} 
                        onClick={() => this.onFirstElementClick()}
                        alt="logo"/>
                </div>;
    }

    renderSecondElement(){
        return <object className="second-element" 
                        ref="secondElement"
                        id="svgObject"
                        data={orrmottaLogo} 
                        type="image/svg+xml">
                    Your browser doesn't support SVG
                </object>;
    }

    render() {
        return (
            <div className="cover-page-wrapper">
                <div ref='container' className="cover-page">
                    <div className="logo-container">
                        <div className="logo-wrapper">
                            {this.renderFirstElement()}
                            {this.renderSecondElement()}
                        </div>
                        <div ref="logoSubtitle" className='logo-subtitle'>
                            <h2>software developer</h2>
                        </div>
                    </div>
                </div>
                 <div className="next-logo-wrapper" ref="nextButton" 
                        onClick={() => this.onNextClick()}>
                    <svg className='next-logo' xmlns="http://www.w3.org/2000/svg" 
                            width="50" height="50" viewBox="0 0 94 94">
                        <circle id='circle' className="next-logo-circle" 
                            stroke="#212121" cx="47" cy="47" r="45"/>
                        <path id='chevron' className="next-logo-chevron" 
                            fill="#212121" d="M69.5,38.39l-5.29-5.29L47,50.28,29.79,33.11,24.5,38.39,47,60.89Z"/>
                    </svg>
                </div>
            </div>
        );
    }
}

export default Cover;
