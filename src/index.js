import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import firebase from 'firebase';

// Initialize Firebase
var ENV = 'development';
var config;
if(ENV == 'production'){
  config = {
    apiKey: "AIzaSyDd4_CRip37D2N1_8G4ay2sDhJw6-fDuwc",
    authDomain: "igorrmotta-portofolio.firebaseapp.com",
    databaseURL: "https://igorrmotta-portofolio.firebaseio.com",
    storageBucket: "igorrmotta-portofolio.appspot.com",
  };
} else {
config = {
    apiKey: "AIzaSyAdYn2mcnWsqgwQX4C-XFp1YwcVWtBf9PI",
    authDomain: "igorrmotta-portfolio-dev.firebaseapp.com",
    databaseURL: "https://igorrmotta-portfolio-dev.firebaseio.com",
    storageBucket: "igorrmotta-portfolio-dev.appspot.com",
  };
}
firebase.initializeApp(config);

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
